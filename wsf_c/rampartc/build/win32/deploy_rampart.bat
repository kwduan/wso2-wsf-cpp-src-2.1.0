@echo off
echo -------------------------------------------------------------------------
echo deploying rampart...
echo -------------------------------------------------------------------------

xcopy .\include %WSFCPP_HOME%\include /E /I /Y /S
xcopy .\lib %WSFCPP_HOME%\lib /E /I /Y /S
xcopy .\modules %WSFCPP_HOME%\modules /E /I /Y /S
xcopy .\samples %WSFCPP_HOME%\samples /E /I /Y /S
xcopy .\services %WSFCPP_HOME%\services /E /I /Y /S
copy .\samples\src\rampartc\data\server_axis2.xml %WSFCPP_HOME%\axis2.xml

cd .\samples\src\rampartc\client
deploy_client_repo.bat
cd ..\..\..\..\

echo -------------------------------------------------------------------------
echo Rampart deployed
echo -------------------------------------------------------------------------
@echo on
