#!/bin/bash
set -e
./autogen.sh
./configure --prefix=${WSFCPP_HOME} --enable-static=no --with-axis2=${WSFCPP_HOME}/include/axis2-1.6.0
make
make install
