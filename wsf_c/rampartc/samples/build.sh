#!/bin/bash
./autogen.sh
./configure --prefix=${WSFCPP_HOME} --enable-static=no --with-axis2=${WSFCPP_HOME}/include/axis2-1.6.0
make
make install
cd client
sh deploy_client_repo.sh
cd ../secpolicy
sh deploy.sh scenario5
cd ../
echo "Copying server's axis2.xml to " $WSFCPP_HOME
cp ./data/server_axis2.xml $WSFCPP_HOME/axis2.xml
echo "samples successfuly build. To run echo client cd to client/sec_echo and run update_n_run.sh"
