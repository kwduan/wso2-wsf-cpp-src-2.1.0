#!/bin/bash
./autogen.sh
./configure --prefix=$WSFCPP_HOME --enable-static=no --with-axis2=${WSFCPP_HOME}/include/axis2-1.6.0
make -j30
make install
