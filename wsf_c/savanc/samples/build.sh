#!/bin/bash

./autogen.sh

./configure --prefix=${WSFCPP_HOME} --with-axis2=${WSFCPP_HOME}/include/axis2-1.6.0 --with-savan=${WSFCPP_HOME}/include/savan-1.0
make -j10
make install
