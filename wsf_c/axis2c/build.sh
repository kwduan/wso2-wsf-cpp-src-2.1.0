#!/bin/bash
set -e
sh autogen.sh
WSFCPP_HOME=${WSFCPP_HOME:=`pwd`/deploy}

export WSFCPP_HOME

echo "WSFCPP_HOME = ${WSFCPP_HOME}"

sh configure --prefix=${WSFCPP_HOME} --enable-tests=yes
make -j 10 
make install

cd samples
sh autogen.sh
sh configure --prefix=${WSFCPP_HOME} --with-axis2=${WSFCPP_HOME}/include/axis2-1.6.0
make -j 10
make install
								
cd ..
