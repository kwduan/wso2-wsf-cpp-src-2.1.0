#!/bin/bash

./autogen.sh

if test -z ${WSFCPP_HOME}
then
    WSFCPP_HOME=`pwd`/../deploy
fi

export WSFCPP_HOME

./configure --prefix=${WSFCPP_HOME} --enable-tests=no 
make
