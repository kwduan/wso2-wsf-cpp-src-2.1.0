#!/bin/bash
if test -z $WSFCPP_HOME; then 
    WSFCPP_HOME=$PWD/../../..
fi
$WSFCPP_HOME/bin/wsclient --soap --xop-in $WSFCPP_HOME/bin/samples/resources/axis2.jpg --xop-out $WSFCPP_HOME/bin/samples/wsclient/ --log-level debug  http://localhost:9090/axis2/services/mtom <$WSFCPP_HOME/bin/samples/wsclient/data/mtom_test.xml
