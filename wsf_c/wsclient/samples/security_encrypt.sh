#!/bin/bash
if test -z $WSFCPP_HOME; then 
    WSFCPP_HOME=$PWD/../../..
fi
$WSFCPP_HOME/bin/wsclient --soap --no-mtom --timestamp --sign-body --key $WSFCPP_HOME/bin/samples/rampart/keys/ahome/alice_key.pem --certificate $WSFCPP_HOME/bin/samples/rampart/keys/ahome/alice_cert.cert --recipient-certificate $WSFCPP_HOME/bin/samples/rampart/keys/ahome/bob_cert.cert --encrypt-payload http://localhost:9090/axis2/services/sec_echo <$WSFCPP_HOME/bin/samples/wsclient/data/echo.xml
