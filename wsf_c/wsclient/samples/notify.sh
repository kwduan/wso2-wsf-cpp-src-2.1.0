#!/bin/bash
if test -z $WSFCPP_HOME; then 
    WSFCPP_HOME=$PWD/../../..
fi
$WSFCPP_HOME/bin/wsclient --soap --send-only --no-mtom --no-wsa http://localhost:9090/axis2/services/notify <$WSFCPP_HOME/bin/samples/wsclient/data/notify.xml
