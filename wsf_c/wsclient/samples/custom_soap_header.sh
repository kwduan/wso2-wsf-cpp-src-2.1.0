#!/bin/bash
if test -z $WSFCPP_HOME; then 
    WSFCPP_HOME=$PWD/../../..
fi
$WSFCPP_HOME/bin/wsclient --soap --no-mtom --no-wsa --soap-header '<dam:Pick xmlns:soapenv="http://www.w3.org/2003/05/soap-envelope" soapenv:mustUnderstand="0" xmlns:dam="http://www.w3.org/2004/09/fly">http://ws.apache.org/axis2/c</dam:Pick>' http://localhost:9090/axis2/services/echo <$WSFCPP_HOME/bin/samples/wsclient/data/echo.xml
