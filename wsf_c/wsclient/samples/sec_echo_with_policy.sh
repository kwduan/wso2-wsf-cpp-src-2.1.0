#!/bin/bash
if test -z $WSFCPP_HOME; then 
    WSFCPP_HOME=$PWD/../../..
fi

INST_DIR=$WSFCPP_HOME
SERVICE_HOME="$INST_DIR/services/sec_echo"

_SMPL_DIR="$PWD"

if [ $# -ne 1 ]
then
    echo "Usage : $0 server-port"
    exit
fi

    _PORT=$1
    echo "-------------------------------------------------------------------------"
    echo "Deploying server with required sec_policy for sec_echo service"

    echo "Replacing sec_echo/services.xml"
    if [ `uname -s` = Darwin ]
    then
        sed -e 's,WSFCPP_HOME,'$INST_DIR',g' -e 's,\.so,\.dylib,g' data/sec_echo_services.xml > $SERVICE_HOME/services.xml
    else
        sed 's,WSFCPP_HOME,'$INST_DIR',g' data/sec_echo_services.xml > $SERVICE_HOME/services.xml
    fi

    killall axis2_http_server
    cd $WSFCPP_HOME/bin
    echo "Start server @ $_PORT"
    ./axis2_http_server -p$_PORT &
    sleep 2
    cd $_SMPL_DIR
    echo "Run the sample"

$WSFCPP_HOME/bin/wsclient --soap --no-mtom --user alice --digest --password password --key /axis2c/deploy/bin/samples/rampart/keys/ahome/alice_key.pem --certificate /axis2c/deploy/bin/samples/rampart/keys/ahome/alice_cert.cert --recipient-certificate /axis2c/deploy/bin/samples/rampart/keys/ahome/bob_cert.cert --policy-file $WSFCPP_HOME/bin/samples/wsclient/data/policy.xml http://localhost:9090/axis2/services/sec_echo <$WSFCPP_HOME/bin/samples/wsclient/data/echo.xml

killall axis2_http_server
echo "DONE"
