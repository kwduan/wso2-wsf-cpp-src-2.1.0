if [ $# -ne 0 ]
then
    echo "Usage : $0"
    echo "Visit http://aws.amazon.com to aget a Amazon key"
    exit
fi
if test -z $WSFCPP_HOME; then 
    WSFCPP_HOME=$PWD/../../..
fi

$WSFCPP_HOME/bin/wsclient --soap1.1 --no-mtom --action http://soap.amazon.com http://soap.amazon.com:80/onca/soap?Service=AWSECommerceService < $WSFCPP_HOME/bin/samples/wsclient/data/amazon_soap_item_search.xml

#$WSFCPP_HOME/bin/wsclient --soap1.1 --no-mtom --action http://soap.amazon.com http://localhost:9090/onca/soap?Service=AWSECommerceService < $WSFCPP_HOME/bin/samples/wsclient/data/item_search.xml
