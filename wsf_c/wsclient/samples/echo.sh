#!/bin/bash
if test -z $WSFCPP_HOME; then 
    WSFCPP_HOME=$PWD/../../..
fi
$WSFCPP_HOME/bin/wsclient --soap --no-mtom --no-wsa http://localhost:9090/axis2/services/echo <$WSFCPP_HOME/bin/samples/wsclient/data/echo.xml
